<?php

if ($file = getenv('APP_SECRETS')) {
    $secrets = json_decode(file_get_contents($file), true);
    if (isset($secrets['MEMCACHE'])) {
        $memcache = $secrets['MEMCACHE'];

        $handlers = [];
        $servers  = [];
        foreach (range(1, $memcache['COUNT']) as $num) {
            $handlers []= $memcache['HOST'. $num]. ':'. $memcache['PORT'. $num];
            $servers []= [
                'host'          => $memcache['HOST'. $num],
                'port'          => $memcache['PORT'. $num],
                'retryInterval' => 2,
                'status'        => true,
                'timeout'       => 2,
                'weight'        => 1,
            ];
        }

        // session config
        ini_set('session.save_handler', 'memcached');
        ini_set('session.save_path', implode(',', $handlers));
        if ($memcache['COUNT'] == 2) {
            ini_set('memcached.sess_number_of_replicas', 1);
            ini_set('memcached.sess_consistent_hash', 1);
            ini_set('memcached.sess_binary', 1);
        }

        // craft config
        return [
            'servers'      => $servers,
            'useMemcached' => true,
        ];
    }
}
