<?php

/**
 * General Configuration
 *
 * All of your system's general configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/general.php
 */

$validationKey = false;
if ($file = getenv('APP_SECRETS')) {
    $secrets = json_decode(file_get_contents($file), true);
    $validationKey = $secrets['CUSTOM']['CRAFT_KEY'];
}

return array(

	'*' => array(
		'allowAutoUpdates' => false,
		'omitScriptNameInUrls' => true,
		'cacheMethod'      => getenv('CRAFT_CACHE') ?: 'db',
		'devMode'           => false,
		'validationKey'    => $validationKey,
		'siteUrl' => 'https://www.proactivity.com.au',
		'timezone' => 'Australia/Melbourne',
        'defaultTemplateExtensions' => array('html', 'twig', 'html.twig'),
	),

	// Ryan's development environment overrides

	'localhost' => array(
		'omitScriptNameInUrls' => false,
		'devMode' => true,
		'testToEmailAddress' => 'ryan@edgetrend.com',
		'useCompressedJs' => false,
		'siteUrl' => 'http://localhost:3650',
		'suppressTemplateErrors' => false,
        'defaultTemplateExtensions' => array('html', 'twig', 'html.twig'),
	),

	// Jack & Liz's development environment overrides

	'dev.proactivity.com.au' => array(
		'omitScriptNameInUrls' => true,
		'devMode' => true,
		'testToEmailAddress' => 'lizsterine@gmail.com',
		'useCompressedJs' => false,
		'siteUrl' => 'http://dev.proactivity.com.au',
		'suppressTemplateErrors' => false,
        'defaultTemplateExtensions' => array('html', 'twig', 'html.twig'),
	)

);
