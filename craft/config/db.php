<?php

/**
 * Database Configuration.
 *
 * All of your system's database configuration settings go in here.
 * You can see a list of the default settings in craft/app/etc/config/defaults/db.php
 */

// Load FortRabbit secrets file
if ($file = getenv('APP_SECRETS')) {
    $secrets = json_decode(file_get_contents($file), true);
    $secrets['MYSQL'] = array_key_exists('MYSQL', $secrets) ? $secrets['MYSQL'] : array();
}

return array(

    '*' => array(

        // The prefix to use when naming tables. This can be no more than 5 characters.
        'tablePrefix' => 'craft',

        // The database server name or IP address. Usually this is 'localhost' or '127.0.0.1'.
        'server' => array_key_exists('HOST', $secrets['MYSQL']) ? $secrets['MYSQL']['HOST'] : 'localhost',

        // The name of the database to select.
        'database' => array_key_exists('DATABASE', $secrets['MYSQL']) ? $secrets['MYSQL']['DATABASE'] : 'proactivity',

        // The database username to connect with.
        'user' => array_key_exists('USER', $secrets['MYSQL']) ? $secrets['MYSQL']['USER'] : 'proactivity',

        // The database password to connect with.
        'password' => array_key_exists('PASSWORD', $secrets['MYSQL']) ? $secrets['MYSQL']['PASSWORD'] : '',

    ),

    // Ryan's development environment overrides

    'localhost' => array(

        // The database server name or IP address. Usually this is 'localhost' or '127.0.0.1'.
        'server' => '127.0.0.1',

        // The name of the database to select.
        'database' => 'proactivity',

        // The database username to connect with.
        'user' => 'dev',

        // The database password to connect with.
        'password' => 'password',

    ),

    // Jack & Liz's development environment overrides

    'dev.proactivity.com.au' => array(

        // The database server name or IP address. Usually this is 'localhost' or '127.0.0.1'.
        'server' => 'localhost',

        // The name of the database to select.
        'database' => 'proactivity',

        // The database username to connect with.
        'user' => 'root',

        // The database password to connect with.
        'password' => '',

    )

);
