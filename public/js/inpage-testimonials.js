$(function(e) {
    var loadMoreBtn = $('<a class="testimonials--load-more">Show All <i class="fa fa-chevron-down"></i></a>')
        .on('click', function(e){
            var show = !$('.testimonials--in-page').hasClass('show-all');

            $('.testimonials--in-page')[(show ? 'add' : 'remove') + 'Class']('show-all');
            $(this).html(show ? 'Show less <i class="fa fa-chevron-up"></i>' : 'Show all <i class="fa fa-chevron-down"></i>');
        });
    $('.testimonials--in-page').append(loadMoreBtn);
});