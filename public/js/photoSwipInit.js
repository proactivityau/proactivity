$(function(e) {
    var $gallery = $('.photo-gallery');

    if($gallery.length) {
        var items = [],
            item,
            photoSize,

            openPhotoSwipe = function(index) {
                var pswpElement = document.querySelectorAll('.pswp')[0],
                    gallery,
                    options;

                // define options (if needed)
                options = {
                    index: index,

                    closeEl:true,
                    fullscreenEl: false,
                    shareEl: false,
                    showHideOpacity:true,

                    // define gallery index (for URL)
                    galleryUID: $gallery.attr('data-pswp-uid'),

                    getThumbBoundsFn: function (index) {
                        // See Options -> getThumbBoundsFn section of documentation for more info
                        var thumbnail = items[index].el.getElementsByTagName('img')[0], // find thumbnail
                            pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
                            rect = thumbnail.getBoundingClientRect();

                        return {x: rect.left, y: rect.top + pageYScroll, w: rect.width, h: rect.height};
                    }

                };

                // exit if index not found
                if (isNaN(options.index)) {
                    return;
                }

                // Pass data to PhotoSwipe and initialize it
                gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options);
                gallery.init();
            };

        $gallery.find('.photo-gallery-item').each(function(e) {
            photoSize = $(this).attr('data-size').split('x');
            item = {
                src: $(this).attr('href'),
                el: $(this)[0],
                w: parseInt(photoSize[0], 10),
                h: parseInt(photoSize[1], 10)
            };
            items.push(item);

            $(this).on('click', function(e) {
                e = e || window.event;
                e.preventDefault ? e.preventDefault() : e.returnValue = false;
                openPhotoSwipe($(this).index());
                return false;
            });
        });
    }
});