/*
 * Slideshow - Copyright (c) 2010, Edge Trend Pty Ltd
 */

$(function() {
    
    // Set div id
    var divId = 'div.slideshow';
    
    // Hide all slideshow images and set position
    $(divId + ' > img').hide();
    $(divId + ' > img').css('position', 'absolute');
    
    // Show slideshow div
    $(divId).show();
    
    // Show first image and set to active
    $(divId + ' img:first').show();
    $(divId + ' img:first').addClass('active');
    
    // Start slide show
    setInterval(slideshowSlide, 3000);
    
    // Slide
    function slideshowSlide() {
        
	if (stopSlider) {
		return true;
	}
	    
        // Check if active set
        if ($(divId + ' img.active').length) {
            
            // Define current/next image
            var currentImage = 0;
            var nextImage = 1;
            
            // Set counter to zero
            var counter = 0;
            
            // Loop images
            $(divId).children().each(function() {
                
                // Increment counter
                counter = counter + 1;
                
                // Add image id
                $(this).attr('id', 'image' + counter);
                
                // Check if image is current
                if ($(divId + ' img.active').attr('id') == $(this).attr('id')) {
                    
                    // Save current image
                    currentImage = counter;
                    
                }
                
            });
            
            // Check if current image not last
            if (currentImage < counter) {
                
                // Set next image to one after current
                nextImage = currentImage + 1;
                
            }
            
            // Show next image
            $('#image' + currentImage).fadeOut('slow');
            $('#image' + nextImage).fadeIn('slow');
            
            // Set active class
            $('#image' + currentImage).removeClass('active');
            $('#image' + nextImage).addClass('active');
            
        }
        
    }
    
});
