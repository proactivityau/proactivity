# Proactivity Website

Build on [Craft](https://craftcms.com/docs) version 2.6.2780

## Craft Plugins:

* [Sprout SEO](http://sprout.barrelstrengthdesign.com/craft-plugins/seo/docs)
	Note: after updating this plugin, you should remove the credit text
	"<!-- This site is optimised... ->"
	from the `craft/plugins/sproutseo/templates/_special/meta.html` file.
	Otherwise, this will output in the _<head_> of all pages!

* [Sprout Forms](http://sprout.barrelstrengthdesign.com/craft-plugins/forms/docs)

* [StdErr Logger](https://github.com/ostark/craft-stderr-logger)
	Note: this should only be used in production. It allows us to view Craft errors 
	using the standard FortRabbit logging tools.

## Deployment

Deployed to [FortRabbit](https://dashboard.fortrabbit.com/apps/proactivity) PaaS, hosted behind [CloudFlare](https://www.cloudflare.com/a/overview/proactivity.com.au) CDN/WAF.
